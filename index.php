<?php
    define('ROOT_PATH', dirname(__FILE__));
    require_once 'autoload.php';    
    require_once 'aplication/function/carregaPage.php';   
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $titulo; ?></title>
         <!-- Bootstrap CSS CDN -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- Our Custom CSS -->
        <link rel="stylesheet" href="assets/css/style4.css">
        <link rel="shortcut icon" href="favicon.png"/>
    </head>
    <body>



        <div class="wrapper">
            <!-- Sidebar Holder -->
            <nav id="sidebar">
                <div class="sidebar-header">
                    <img src="assets/img/logoself.png" width="40" heigth="40" alt="">
                    <h3>SELF - EPI Manager</h3>
                    <strong>SELF</strong>
                </div>

                <ul class="list-unstyled components">
                    <li class="active">
                        <a href="./index.php?op=dashboard">
                            <i class="glyphicon glyphicon-home"></i>
                            Dashboard
                        </a>
                        
                    </li>
                    
                    <li>
                        <a href="./index.php?op=epi">
                            <i class="glyphicon glyphicon-briefcase"></i>
                            EPI
                        </a>
                        <!--<a href="./home.php?p=2" data-toggle="collapse" aria-expanded="false">
                            <i class="glyphicon glyphicon-duplicate"></i>
                            Funcionários
                        </a>
                        <ul class="collapse list-unstyled" id="pageSubmenu">
                            <li><a href="#">Page 1</a></li>
                            <li><a href="#">Page 2</a></li>
                            <li><a href="#">Page 3</a></li>
                        </ul>-->
                    </li>
                    <li>
                        <a href="./index.php?op=funcionario">
                            <i class="glyphicon glyphicon-duplicate"></i>
                            Funcionários
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="glyphicon glyphicon-link"></i>
                            Alm Hig
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="glyphicon glyphicon-paperclip"></i>
                            Equip Geral
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="glyphicon glyphicon-send"></i>
                            Dep Extremoz
                        </a>
                    </li>
                </ul>

                
            </nav>

            <!-- Page Content Holder -->
            <div id="content">
             <?php 
                include_once 'aplication/view/header-nav.php';                
                include_once $conteudo;              
                ?>
            </div>
            <footer>
                
            </footer>
        </div>





        <!-- jQuery CDN -->
         <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
         <!-- Bootstrap Js CDN -->
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

         <script type="text/javascript">
             $(document).ready(function () {
                 $('#sidebarCollapse').on('click', function () {
                     $('#sidebar').toggleClass('active');
                 });
             });
         </script>
    </body>
</html>
