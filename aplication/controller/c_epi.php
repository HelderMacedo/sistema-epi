<?php
namespace aplication\controller;
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);
require_once '../../autoload.php';

$epi = new Epi();

if(isset($_POST['descricao']) && !empty($_POST['descricao'])){
    $descricao = $_POST['descricao'];
    $rastreamento = $_POST['rastreamento'];
    $unidade = $_POST['unidade'];
    $marca = $_POST['marca'];
    $valorUni = $_POST['valor'];
    $quantidade = $_POST['quantidade'];
    $total = $_POST['total'];

    $epi->setDescricao($descricao);
    $epi->setNumeroRastreamento($rastreamento);
    $epi->setUnidade($unidade);
    $epi->setMarca($marca);
    $epi->setValorUnitario($valorUni);
    $epi->setQuantidade($quantidade);
    $epi->setValorTotal($total);
    
    $epi->insert();
    
    header('Location: ../../index.php?op=epi');
    
    }    

    function objListar(){
       return $epi->finAll();
    }
?>


