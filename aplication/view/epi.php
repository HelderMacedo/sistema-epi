
<div class="row">
    <div class="col-md-4">
        <a href="#" data-toggle="modal" data-target="#modalCadastrar">
            <div class="layers bd bgc-white p-20">            
                <img src="assets/img/cadastrarEpi.png" alt=""> 
                <span>Cadastrar Epi</span>                         
            </div>           
        </a>
    </div>
    <div class="col-md-4">
        <a href="#">
            <div class="layers bd bgc-white p-20">
                <img src="assets/img/ficha.png" alt="">
                <span>Ficha de Epi</span>
            </div>
        </a>
    </div>
    <div class="col-md-4">
        <a href="#">
            <div class="layers bd bgc-white p-20">
            <span>cadastrar</span>
            </div>
        </a>
    </div>

   
</div>

<!--Listar  -->
<div class="divTable">
    <table class="table table-striped">
    <thead>
        <tr>
            <th scope="col">Número patrimônio</th>
            <th scope="col">Descrição</th>
            <th scope="col" class="thcenter">Unidade</th>
            <th scope="col" class="thcenter">Quantidade</th>
            <th scope="col" class="thcenter">Marca</th>
            <th scope="col">Valor Unitario</th>
            <th scope="col">Valor Total</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <?php foreach($epi->findAll() as $key => $valor):?>
                <tr>
                    <td><?=$valor->numero_rastreamento;?></td>
                    <td><?=$valor->descricao;?></td>
                    <td class="tdcenter"><?=$valor->unidade;?></td>
                    <td class="tdcenter"><?=$valor->quantidade;?></td>
                    <td class="tdcenter"><?=$valor->marca;?></td>
                    <td class="tdcenter"><?=$valor->valor_unitario;?></td>
                    <td class="tdcenter"><?=$valor->valor_total;?></td>
                </tr>
            <?php endforeach; ?>
        </tr>    
    </tbody>
    </table>
</div>


<!--Modal -->

<div class="modal fade" id="modalCadastrar" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
                <h3 class="modal-title">Cadastrar</h3>
                
            </div>
            <div class="modal-body">
                <form role="form" method="POST" action="aplication/controller/c_epi.php">
                    <input type="hidden" name="_token" value="">
                    <div class="form-row">
                        <div class="form-group col-sm-6">
                            <label for="descricao" class="control-label">Descrição</label>                    
                            <input type="text" class="form-control" name="descricao" value="">                    
                        </div>
                        <div class="form-group col-sm-3">
                            <label for="ratreamento" class="control-label">Patrimônio</label>
                            <input type="number" min="0" class="form-control" name="rastreamento" value="">                        
                        </div>
                        <div class="form-group col-sm-3">
                            <label for="unidade" class="control-label">Unidade</label>
                            <input type="text" class="form-control" name="unidade" value="">                        
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-sm-3">
                            <label for="marca" class="control-label">Marca</label>
                            <input type="text" class="form-control" name="marca" value="">                        
                        </div>
                        <div class="form-group col-sm-3">
                            <label for="valor" class="control-label">Valor Unitário</label>
                            <input type="number" step="any" min="0" id="valorUnitario" class="form-control" name="valor" value="">                        
                        </div>                
                        <div class="form-group col-sm-3">
                            <label for="quantidade" class="control-label">Quantidade</label>
                            <input type="number" id="quantidade" class="form-control" name="quantidade" value="">                        
                        </div>                        
                        
                        <div class="form-group col-sm-3">
                            <label for="total" class="control-label">Valor Total</label>
                            <input type="number" step="any" min="0" id="total" class="form-control" name="total" value="">                        
                        </div>
                    </div> 
                    <div class="form-group">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        <button type="submit" name="cadastrar" class="btn btn-success" onclick="alert('Cadastrado com sucesso')">Cadastrar</button>
                    </div>        

                </form>
            </div>            
        </div>
    </div>    
</div>
<!--Fim Modal -->
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#quantidade").change(function(){
            var qtd = $(this).val();
            var valor = $("#valorUnitario").val();
            var total = valor * qtd;
            $("#total").val(total);
        });
    });

</script>