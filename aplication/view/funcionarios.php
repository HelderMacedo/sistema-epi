<?php
    require_once 'autoload.php';
?>
<div class="row">
    <div class="col-md-4">
        <a href="#">
            <div class="layers bd bgc-white p-20">
                <h3>Cadastrar</h3>
            </div>
        </a>
    </div>
    <div class="col-md-4">
        <a href="#">
            <div class="layers bd bgc-white p-20">
                <h3>Listar</h3>
            </div>
        </a>
    </div>
    <div class="col-md-4">
        <a href="">
            <div class="layers bd bgc-white p-20">
                <h3>deletar</h3>
            </div>
        </a>
    </div>
</div>
<?php
    $funcionarios = new Funcionarios();
    if(isset($_POST['cadastrar'])){
        $nome = addslashes($_POST['nome']);
        $funcao = addslashes($_POST['funcao']);
        
        $funcionarios->setNome($nome);
        $funcionarios->setFuncao($funcao);
        if($funcionarios->insert()){
            echo "Inserido com sucesso!";
        }
    }
    
?>
<br/><br/>
<form method="post" action="">
	<div class="input-prepend">
		<span class="add-on"><i class="icon-user"></i></span>
		<input type="text" name="nome" placeholder="Nome:" />
	</div>
    <br/>
	<div class="input-prepend">
		<span class="add-on"><i class="icon-envelope"></i></span>
		<input type="text" name="funcao" placeholder="Função" />
	</div>
	<br />
	<input type="submit" name="cadastrar" class="btn btn-primary" value="Cadastrar dados">					
</form>
<h2>SELF - EPI Manager</h2>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

<div class="line"></div>