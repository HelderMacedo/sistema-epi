<?php
require_once 'Crud.php';

class Funcionarios extends Crud{
    protected $table = 'funcionarios';
    private $nome;
    private $funcao;

    public function setNome($nome){
        $this->nome = $nome;
    }

    public function getNome(){
        return $this->nome;
    }

    public function setFuncao($funcao){
        $this->funcao = $funcao;
    }

    public function getFuncao(){
        return $this->funcao;
    }

    public function insert(){
        $sql = "INSERT INTO $this->table (nome, funcao) VALUES(:nome, :funcao)";
        $stmt = DB::prepare($sql);
        $stmt->bindValue(":nome",$this->nome);
        $stmt->bindValue(":funcao",$this->funcao);
        return $stmt->execute();
    }

    public function update($id){
        $sql = "UPDATE $this->table SET nome = :nome, funcao = :funcao WHERE id = :id";
        $stmt = DB::prepare($sql);
        $stmt->bindValue(":nome",$this->nome);
        $stmt->bindValue(":funcao",$this->funcao);
        $stmt->bindValue(":id",$id);
        return $stmt->execute();
    }
}

?>