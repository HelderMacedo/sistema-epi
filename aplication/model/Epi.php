<?php
   
    require_once 'Crud.php';
    class Epi extends Crud{
        protected $table = 'epi';
        private $numero_rastreamento;
        private $descricao;
        private $unidade;
        private $quantidade;
        private $marca;
        private $valor_unitario;
        private $valor_total;

        public function getNumeroRastreamento(){
           return $this->numero_rastreamento;
        }

        public function setNumeroRastreamento($numero_rastreamento){
            $this->numero_rastreamento = $numero_rastreamento;
        }

        public function getDescricao(){
            return $this->descricao;
         }
 
        public function setDescricao($descricao){
            $this->descricao = $descricao;
        }

        public function getUnidade(){
            return $this->unidade;
        }
 
        public function setUnidade($unidade){
            $this->unidade = $unidade;
        }

        public function getQuantidade(){
            return $this->quantidade;
        }
 
        public function setQuantidade($quantidade){
            $this->quantidade = $quantidade;
        }

        public function getMarca(){
            return $this->marca;
        }
 
        public function setMarca($marca){
            $this->marca = $marca;
        }

        public function getValorUnitario(){
            return $this->valor_unitario;
        }
 
        public function setValorUnitario($valor_unitario){
            $this->valor_unitario = $valor_unitario;
        }

        public function getValorTotal(){
            return $this->valor_total;
        }
 
        public function setValorTotal($valor_total){
            $this->valor_total = $valor_total;
        }


        public function insert(){
            $sql = ("INSERT INTO $this->table (numero_rastreamento, descricao, unidade, quantidade, marca, valor_unitario, valor_total)
                    VALUES(:rastreamento, :descricao, :unidade, :quantidade, :marca, :valorUni ,:valor_total)");
            $stmt = DB::prepare($sql);
            $stmt->bindValue(":rastreamento",$this->numero_rastreamento);
            $stmt->bindValue(":descricao",$this->descricao);
            $stmt->bindValue(":unidade",$this->unidade);
            $stmt->bindValue(":quantidade",$this->quantidade);
            $stmt->bindValue(":marca",$this->marca);
            $stmt->bindValue(":valorUni",$this->valor_unitario);
            $stmt->bindValue(":valor_total",$this->valor_total);
            return $stmt->execute();

        }

        public function update($id){
            $sql = "UPDATE $this->table SET numero_rastreamento = :rastreamento, descricao = :descricao, unidade = :unidade, quantidade = :quantidade, marca = :marca, valor_unitario = :valorUni, valor_total = :valor_total WHERE id = :id";
            $stmt = DB::prepare($sql);
            $stmt->bindValue(":rastreamento",$this->numero_rastreamento);
            $stmt->bindValue(":descricao",$this->descricao);
            $stmt->bindValue(":unidade",$this->unidade);
            $stmt->bindValue(":quantidade",$this->quantidade);
            $stmt->bindValue(":marca",$this->marca);
            $stmt->bindValue(":valorUni",$this->valor_unitario);
            $stmt->bindValue(":valor_total",$this->valor_total);
            $stmt->bindValue(":id",$id);
            return $stmt->execute();
        }

    }
?>